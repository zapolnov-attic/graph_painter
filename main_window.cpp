/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//                    Dmitry Antonov (dmitry@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "main_window.h"
#include "graphics_view.h"
#include <QGraphicsScene>
#include <QSettings>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow()
{
	setupUi(this);

	QSettings settings;
	m_FileName = settings.value("filename").toString();
	m_ImageName = settings.value("imagename").toString();
	int width = settings.value("width", 1024).toInt();
	int height = settings.value("height", 768).toInt();
	uiWidthSpin->setValue(width);
	uiHeightSpin->setValue(height);

	uiGraphicsView->setFocus();
}

void MainWindow::on_uiWidthSpin_valueChanged(int value)
{
	QSettings settings;
	settings.setValue("width", value);

	QRectF r = uiGraphicsView->sceneRect();
	r.setWidth(static_cast<qreal>(value));
	uiGraphicsView->setSceneRect(r);
	uiGraphicsView->scene()->setSceneRect(r);
}

void MainWindow::on_uiHeightSpin_valueChanged(int value)
{
	QSettings settings;
	settings.setValue("height", value);

	QRectF r = uiGraphicsView->sceneRect();
	r.setHeight(static_cast<qreal>(value));
	uiGraphicsView->setSceneRect(r);
	uiGraphicsView->scene()->setSceneRect(r);
}

void MainWindow::on_uiNewButton_clicked()
{
	uiGraphicsView->initNew();
}

void MainWindow::on_uiOpenButton_clicked()
{
	QString file = QFileDialog::getOpenFileName(this, QString(), m_FileName, tr("Graph files (*.graph)"));
	if (file.length() != 0)
	{
		QFile f(file);
		if (!f.open(QFile::ReadOnly))
		{
			QMessageBox::critical(this, tr("Error"), tr("Unable to open file '%1'.").arg(file));
			return;
		}
		uiGraphicsView->initFromString(f.readAll().constData());
		f.close();

		m_FileName = file;

		QSettings settings;
		settings.setValue("filename", m_FileName);
	}
}

void MainWindow::on_uiSaveButton_clicked()
{
	QString file = QFileDialog::getSaveFileName(this, QString(), m_FileName, tr("Graph files (*.graph)"));
	if (file.length() != 0)
	{
		std::string str = uiGraphicsView->toString();

		QFile f(file);
		if (!f.open(QFile::WriteOnly))
		{
			QMessageBox::critical(this, tr("Error"), tr("Unable to create file '%1'.").arg(file));
			return;
		}
		f.write(str.data(), static_cast<qint64>(str.length()));
		f.close();

		m_FileName = file;

		QSettings settings;
		settings.setValue("filename", m_FileName);
	}
}

void MainWindow::on_uiOpenImageButton_clicked()
{
	QString file = QFileDialog::getOpenFileName(this, QString(), m_ImageName,
		tr("Images (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm *.tiff *.xbm *.xpm)"));
	if (file.length() != 0)
	{
		QImage & image = uiGraphicsView->backgroundImage();
		if (!image.load(file))
		{
			QMessageBox::critical(this, tr("Error"), tr("Unable to load image '%1'.").arg(file));
			return;
		}

		uiGraphicsView->resetCachedContent();
		uiGraphicsView->scene()->update(uiGraphicsView->scene()->sceneRect());
		uiGraphicsView->scene()->setBackgroundBrush(Qt::gray);
		uiGraphicsView->update();

		m_ImageName = file;

		QSettings settings;
		settings.setValue("imagename", m_ImageName);
	}
}
