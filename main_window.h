/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//                    Dmitry Antonov (dmitry@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __7aced395503d6f5c8485950d686d3875__
#define __7aced395503d6f5c8485950d686d3875__

#include <QWidget>
#include "ui_main_window.h"

class GraphicsView;
class QGraphicsScene;

class MainWindow : public QWidget, private Ui_MainWindow
{
	Q_OBJECT

public:
	MainWindow();

private:
	QString m_FileName;
	QString m_ImageName;

	Q_SLOT void on_uiWidthSpin_valueChanged(int value);
	Q_SLOT void on_uiHeightSpin_valueChanged(int value);

	Q_SLOT void on_uiNewButton_clicked();
	Q_SLOT void on_uiOpenButton_clicked();
	Q_SLOT void on_uiSaveButton_clicked();

	Q_SLOT void on_uiOpenImageButton_clicked();
};

#endif
