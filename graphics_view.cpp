/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//                    Dmitry Antonov (dmitry@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#include "graphics_view.h"
#include "lua/lua.hpp"
#include <QGraphicsItem>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QMessageBox>
#include <sstream>

static const float R = 10;

class Scene : public QGraphicsScene
{
public:
	GraphicsView * view;
	QImage image;
	inline Scene(GraphicsView * parent) : QGraphicsScene(parent), view(parent) {}
	void drawBackground(QPainter * painter, const QRectF &) { painter->drawImage(view->sceneRect(), image); }
};

GraphicsView::GraphicsView(QWidget * parent)
	: QGraphicsView(parent),
	  m_DraggingItem(NULL),
	  m_DraggingLine(NULL)
{
	setScene(new Scene(this));
}

void GraphicsView::initNew()
{
	m_Lines.clear();
	m_Edges.clear();
	m_LineEdges.clear();
	m_Dynamic.clear();
	scene()->clear();
	update();
}

std::string GraphicsView::toString() const
{
	std::stringstream ss;

	ss << "return {\n";

	ss << "\tweb_points = {\n";
	size_t id = 0;
	std::map<QGraphicsItem *, size_t> points;
	for (LineMap::const_iterator it = m_Lines.begin(); it != m_Lines.end(); ++it)
	{
		QPointF p = it->first->pos();
		ss << "\t\t{ " << p.x() << ", " << p.y() << ", \""
			<< (m_Dynamic[it->first] ? "dynamic" : "static") << "\" },\n";
		points.insert(std::make_pair(it->first, ++id));
	}
	ss << "\t},\n";

	ss << "\tweb_connections = {\n";
	for (std::set<std::pair<QGraphicsItem *, QGraphicsItem *> >::const_iterator
			it = m_Edges.begin(); it != m_Edges.end(); ++it)
		ss << "\t\t{ " << points[it->first] << ", " << points[it->second] << " },\n";
	ss << "\t},\n";

	ss << "}\n";

	return ss.str();
}

void GraphicsView::initFromString(const std::string & str)
{
	initNew();

	lua_State * L = luaL_newstate();
	try
	{
		int status = luaL_loadbuffer(L, str.c_str(), str.length(), "");
		if (status != 0)
		{
			QMessageBox::critical(this, tr("Error"), tr("Invalid lua code."));
			return;
		}

		status = lua_pcall(L, 0, 1, 0);
		if (status != 0)
		{
			QMessageBox::critical(this, tr("Error"), tr("Invalid lua code."));
			return;
		}

		std::vector<QGraphicsItem *> points;
		lua_getfield(L, -1, "web_points");
		lua_pushnil(L);
		while (lua_next(L, -2))
		{
			lua_rawgeti(L, -1, 1);
			lua_Number x = lua_tonumber(L, -1);
			lua_pop(L, 1);

			lua_rawgeti(L, -1, 2);
			lua_Number y = lua_tonumber(L, -1);
			lua_pop(L, 1);

			lua_rawgeti(L, -1, 3);
			bool isDynamic = !strcmp(lua_tostring(L, -1), "dynamic");
			lua_pop(L, 1);

			QGraphicsEllipseItem * item = scene()->addEllipse(-R, -R, R * 2, R * 2);
			item->setPos(x, y);
			item->setBrush(QBrush(isDynamic ? Qt::red : Qt::white));
			points.push_back(item);
			m_Dynamic[item] = isDynamic;

			lua_pop(L, 1);
		}
		lua_pop(L, 1);

		lua_getfield(L, -1, "web_connections");
		lua_pushnil(L);
		while (lua_next(L, -2))
		{
			lua_rawgeti(L, -1, 1);
			lua_Integer idx1 = lua_tointeger(L, -1);
			lua_pop(L, 1);

			lua_rawgeti(L, -1, 2);
			lua_Integer idx2 = lua_tointeger(L, -1);
			lua_pop(L, 1);

			QGraphicsItem * item1 = points[idx1 - 1];
			QGraphicsItem * item2 = points[idx2 - 1];

			QPointF p1 = item1->pos();
			QPointF p2 = item2->pos();

			QGraphicsLineItem * line = scene()->addLine(p1.x(), p1.y(), p2.x(), p2.y(),
				QPen(Qt::white, R, Qt::SolidLine, Qt::RoundCap));

			m_Lines[item1].push_back(std::make_pair(line, true));
			m_Lines[item2].push_back(std::make_pair(line, false));
			m_Edges.insert(std::make_pair(item1, item2));
			m_LineEdges[line] = std::make_pair(item1, item2);

			lua_pop(L, 1);
		}
		lua_pop(L, 1);
	}
	catch (...)
	{
		lua_close(L);
		throw;
	}
	lua_close(L);
}

QImage & GraphicsView::backgroundImage()
{
	return static_cast<Scene *>(scene())->image;
}

void GraphicsView::mousePressEvent(QMouseEvent * event)
{
	QGraphicsItem * item = pointAt(event->pos());
	if (!item)
	{
		item = scene()->addEllipse(-R, -R, R * 2, R * 2);
		static_cast<QGraphicsEllipseItem *>(item)->setBrush(QBrush(Qt::white));
		item->setPos(mapToScene(event->pos()));
	}
	else
	{
		m_DraggingItem = item;
		if (event->button() == Qt::RightButton)
		{
			QPointF p = mapToScene(event->pos());
			m_DraggingLine = scene()->addLine(p.x(), p.y(), p.x(), p.y(),
				QPen(Qt::white, R, Qt::SolidLine, Qt::RoundCap));
			return;
		}
	}
}

void GraphicsView::mouseReleaseEvent(QMouseEvent * event)
{
	if (m_DraggingLine)
	{
		QGraphicsItem * item = pointAt(event->pos());
		if (!item || item == m_DraggingItem)
			scene()->removeItem(m_DraggingLine);
		else
		{
			m_DraggingLine->setLine(QLineF(m_DraggingItem->pos(), item->pos()));
			m_Lines[m_DraggingItem].push_back(std::make_pair(m_DraggingLine, true));
			m_Lines[item].push_back(std::make_pair(m_DraggingLine, false));
			m_Edges.insert(std::make_pair(m_DraggingItem, item));
			m_LineEdges[m_DraggingLine] = std::make_pair(m_DraggingItem, item);
		}
	}

	m_DraggingItem = NULL;
	m_DraggingLine = NULL;
}

void GraphicsView::mouseMoveEvent(QMouseEvent * event)
{
	if (m_DraggingItem)
	{
		QPointF p = mapToScene(event->pos());
		if (m_DraggingLine)
		{
			QLineF l = m_DraggingLine->line();
			l.setP2(p);
			m_DraggingLine->setLine(l);
		}
		else
		{
			m_DraggingItem->setPos(p);
			const LineVector & v = m_Lines[m_DraggingItem];
			for (LineVector::const_iterator it = v.begin(); it != v.end(); ++it)
			{
				QLineF l = it->first->line();
				if (it->second)
					l.setP1(p);
				else
					l.setP2(p);
				it->first->setLine(l);
			}
		}
	}
}

void GraphicsView::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Delete || event->key() == Qt::Key_Backspace)
	{
		QGraphicsItem * item = itemAt(mapFromGlobal(QCursor::pos()));
		if (item)
		{
			QGraphicsLineItem * line = dynamic_cast<QGraphicsLineItem *>(item);
			if (line)
				removeLine(line);
			else
			{
				const LineVector & v = m_Lines[item];
				while (v.size() > 0)
					removeLine(v.front().first);
				scene()->removeItem(item);
				m_Lines.erase(item);
				m_Dynamic.erase(item);
			}
		}
	}
	else if (event->key() == Qt::Key_D)
	{
		QGraphicsItem * item = pointAt(mapFromGlobal(QCursor::pos()));
		if (item)
		{
			QGraphicsEllipseItem * ellipse = dynamic_cast<QGraphicsEllipseItem *>(item);
			if (ellipse)
			{
				m_Dynamic[item] = !m_Dynamic[item];
				ellipse->setBrush(QBrush(m_Dynamic[item] ? Qt::red : Qt::white));
			}
		}
	}
}

QGraphicsItem * GraphicsView::pointAt(QPoint p)
{
	QList<QGraphicsItem *> itemList = items(p);
	for (QList<QGraphicsItem *>::const_iterator it = itemList.begin(); it != itemList.end(); ++it)
	{
		if (!dynamic_cast<QGraphicsLineItem *>(*it))
			return *it;
	}
	return NULL;
}

void GraphicsView::removeLine(QGraphicsLineItem * item)
{
	for (LineMap::iterator it = m_Lines.begin(); it != m_Lines.end(); ++it)
	{
		LineVector & v = it->second;
		for (LineVector::iterator jt = v.begin(); jt != v.end(); )
		{
			if (jt->first != item)
				++jt;
			else
				jt = v.erase(jt);
		}
	}

	std::map<QGraphicsLineItem *, EdgePair>::iterator it = m_LineEdges.find(item);
	if (it != m_LineEdges.end())
	{
		m_Edges.erase(it->second);
		m_LineEdges.erase(it);
	}

	scene()->removeItem(item);
}
