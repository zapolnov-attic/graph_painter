/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (nikolay@friedcroc.com).
//                    Dmitry Antonov (dmitry@friedcroc.com).
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
#ifndef __e405dfad7f10390034cf6f8333734c89__
#define __e405dfad7f10390034cf6f8333734c89__

#include <QGraphicsView>
#include <map>
#include <vector>
#include <string>
#include <set>

class GraphicsView : public QGraphicsView
{
	Q_OBJECT

public:
	GraphicsView(QWidget * parent = 0);

	void initNew();
	std::string toString() const;
	void initFromString(const std::string & str);

	QImage & backgroundImage();

protected:
	void mousePressEvent(QMouseEvent * event);
	void mouseReleaseEvent(QMouseEvent * event);
	void mouseMoveEvent(QMouseEvent * event);
	void keyPressEvent(QKeyEvent * event);

private:
	typedef std::vector<std::pair<QGraphicsLineItem *, bool> > LineVector;
	typedef std::map<QGraphicsItem *, LineVector> LineMap;
	typedef std::pair<QGraphicsItem *, QGraphicsItem *> EdgePair;

	QGraphicsItem * m_DraggingItem;
	QGraphicsLineItem * m_DraggingLine;
	LineMap m_Lines;
	std::set<EdgePair> m_Edges;
	std::map<QGraphicsLineItem *, EdgePair> m_LineEdges;
	mutable std::map<QGraphicsItem *, bool> m_Dynamic;

	QGraphicsItem * pointAt(QPoint p);
	void removeLine(QGraphicsLineItem * item);
};

#endif
